# Advent of Code 2021 - Rust

Solutions written in Rust for Advent of Code 2021. Basically copying the
implementation from my TypeScript solution over on GitHub.

