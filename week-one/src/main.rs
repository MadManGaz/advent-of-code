fn parse_data(str: &str) -> Vec<i32> {
    str.split("\n")
        .map(|str| str.parse::<i32>().expect("Failed to parse string to i32."))
        .collect()
}

fn get_number_of_ping_increases(pings: &Vec<i32>) -> i32 {
    let mut ping_increases = 0;
    for (index, ping) in pings.iter().enumerate() {
        let prev_ping = pings.get(index - 1).unwrap_or(&ping);
        if &prev_ping < &ping {
            ping_increases += 1;
        }
    }
    ping_increases
}

fn get_rolling_window_ping_increases(pings: &Vec<i32>) -> i32 {
    let mut ping_increases = 0;
    for (index, ping) in pings.windows(3).into_iter().enumerate() {
        let ping_sum = &ping.iter().reduce(|a, b| a + b);
    }
    ping_increases
}

fn main() {
    let submarine_pings = parse_data(include_str!("data/data.txt"));
    let num_of_ping_distance_increases = get_number_of_ping_increases(&submarine_pings);
    let num_of_window_ping_increases = get_rolling_window_ping_increases(&submarine_pings);
    println!(
        "number of increased pings: {}",
        num_of_ping_distance_increases
    );
    println!(
        "number of windowed ping increases: {}",
        num_of_window_ping_increases
    )
}
